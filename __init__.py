from flask import Flask
from defaults import defaults
from flask_sqlalchemy import SQLAlchemy
from flask.ext.script import Manager

app = Flask(__name__)

app.config.from_object(defaults)

db = SQLAlchemy(app)
manager = Manager(app)

if __name__ == '__main__':
    manager.run()
