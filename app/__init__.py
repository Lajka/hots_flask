from app import defaults
from flask import Flask, g
from flask.ext.bootstrap import Bootstrap
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config.from_object(defaults)
db = SQLAlchemy(app)
