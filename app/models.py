from app import db
from datetime import datetime

hero_talents = db.Table('hero_talents', 
        db.Column('hero_id', db.Integer, db.ForeignKey('heroes.id')),
        db.Column('talent_id', db.Integer, db.ForeignKey('talents.id'))
)


class Hero(db.Model):
    __tablename__ = 'heroes'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    role = db.Column(db.String(50), nullable=False)
    description = db.Column(db.Text, nullable=False)
    lore = db.Column(db.Text, nullable=False)
    hp = db.Column(db.Integer, nullable=False)
    hp_per_level = db.Column(db.Integer, nullable=False)
    hp_regen = db.Column(db.Float, nullable=False)
    hp_regen_per_level = db.Column(db.Float, nullable=False)
    mana = db.Column(db.Integer, nullable=False)
    mana_per_level = db.Column(db.Integer, nullable=False)
    mana_regen = db.Column(db.Float, nullable=False)
    mana_regen_per_level = db.Column(db.Float, nullable=False)
    attack_damage = db.Column(db.Integer, nullable=False)
    attack_damage_per_level = db.Column(db.Integer, nullable=False)
    attack_speed = db.Column(db.Float, nullable=False)
    talents = db.relationship('Talent', secondary=hero_talents, 
            backref=db.backref('heroes', lazy='dynamic'))

    def __init__(self, name, role, description, lore, hp, hp_per_level, 
                 hp_regen, hp_regen_per_level, mana, mana_per_level, 
                 mana_regen, mana_regen_per_level, attack_damage, 
                 attack_damage_per_level, attack_speed):
        self.name = name
        self.role = role
        self.description = description
        self.lore = lore
        self.hp = hp
        self.hp_per_level = hp_per_level
        self.hp_regen = hp_regen
        self.hp_regen_per_level = hp_regen_per_level
        self.mana = mana
        self.mana_per_level = mana_per_level
        self.mana_regen = mana_regen
        self.mana_regen_per_level = mana_regen_per_level
        self.attack_damage = attack_damage
        self.attack_damage_per_level = attack_damage_per_level
        self.attack_speed = attack_speed

    def __repr__(self):
        return u'<Hero %d : %s>' % (self.id, self.name)


class Talent(db.Model):
    __tablename__ = 'talents'
    id = db.Column(db.Integer, primary_key=True)
    lvl = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(60), nullable=False)
    description = db.Column(db.Text, nullable=False)

    def __init__(self, lvl, name, description):
        self.lvl = lvl
        self.name = name
        self.description = description

    def __repr__(self):
        return u'<Talent %d : %s' % (self.id, self.name)


class HeroicAbility(db.Model):
    __tablename__ = 'heroic_abilities'
    id = db.Column(db.Integer, primary_key=True)
    hero_id = db.Column(db.Integer, db.ForeignKey('heroes.id'), nullable=False)
    name = db.Column(db.String(60), nullable=False)
    description = db.Column(db.Text, nullable=False)
    manacost = db.Column(db.Integer)
    cooldown = db.Column(db.Integer)
    type = db.Column(db.String(50))
    key = db.Column(db.String(1))

    def __init__(self, hero_id, name, description, manacost=None, cooldown=None, 
                 type=None, key=None):
        self.hero_id = hero_id
        self.name = name
        self.description = description
        self.manacost = manacost
        self.cooldown = cooldown
        self.type = type
        self.key = key

    def __repr__(self):
        return u'<Heroic Ability %d : %s>' % (self.id, self.name)


class Trait(db.Model):
    __tablename__ = 'traits'
    id = db.Column(db.Integer, primary_key=True)
    hero_id = db.Column(db.Integer, db.ForeignKey('heroes.id'), nullable=False)
    name = db.Column(db.String(60), nullable=False)
    description = db.Column(db.Text, nullable=False)
    manacost = db.Column(db.Integer)
    cooldown = db.Column(db.Integer)
    type = db.Column(db.String(50))
    key = db.Column(db.String(1))

    def __init__(self, hero_id, name, description, manacost=None, cooldown=None, 
                 type=None, key=None):
        self.hero_id = hero_id
        self.name = name
        self.description = description
        self.manacost = manacost
        self.cooldown = cooldown
        self.type = type
        self.key = key

    def __repr__(self):
        return u'<Trait %d : %s>' % (self.id, self.name)

class Ability(db.Model):
    __tablename__ = 'abilities'
    id = db.Column(db.Integer, primary_key=True)
    hero_id = db.Column(db.Integer, db.ForeignKey('heroes.id'), nullable=False)
    name = db.Column(db.String(60), nullable=False)
    description = db.Column(db.Text, nullable=False)
    manacost = db.Column(db.Integer)
    cooldown = db.Column(db.Integer)
    type = db.Column(db.String(50))
    key = db.Column(db.String(1))

    def __init__(self, hero_id, name, description, manacost=None, cooldown=None, 
                 type=None, key=None):
        self.hero_id = hero_id
        self.name = name
        self.description = description
        self.manacost = manacost
        self.cooldown = cooldown
        self.type = type
        self.key = key

    def __repr__(self):
        return u'<Ability %d : %s>' % (self.id, self.name)
