from app import app, models, db, forms
from flask import g, render_template, redirect, url_for, flash, request, session
import os

@app.route('/')
def index():
    return redirect(url_for('heroes'))


@app.route('/form')
def form_test():
    name = None
    role = None
    form = forms.TestForm()
    if form.validate_on_submit():
        name            = form.name.data
        form.name.data  = ''
        role            = form.role.date
        form.role.data  = ''
    return render_template('formtest.html', form=form, name=name, role=role)

@app.route('/heroes')
def heroes():
    heroes = None
    heroes = models.Hero.query.order_by(models.Hero.name)
    return render_template('heroes.html', heroes=heroes)

@app.route('/addhero', methods=['GET', 'POST'])
def add_hero():
    form = forms.AddHeroForm()

    if form.validate_on_submit():
        name                    = form.name.data
        role                    = form.role.data
        description             = form.description.data
        lore                    = form.lore.data
        hp                      = form.hp.data
        hp_per_level            = form.hp_per_level.data
        hp_regen                = form.hp_regen.data
        hp_regen_per_level      = form.hp_regen_per_level.data
        mana                    = form.mana.data
        mana_per_level          = form.mana_per_level.data
        mana_regen              = form.mana_regen.data
        mana_regen_per_level    = form.mana_regen_per_level.data
        attack_damage           = form.attack_damage.data
        attack_damage_per_level = form.attack_damage_per_level.data
        attack_speed            = form.attack_speed.data

        hero = models.Hero(name, role, description, lore, hp, hp_per_level, 
                           hp_regen, hp_regen_per_level, mana, mana_per_level, 
                           mana_regen, mana_regen_per_level, attack_damage, 
                           attack_damage_per_level, attack_speed)
        db.session.add(hero)
        db.session.commit()
        flash(u'Hero successfully added!')

        return redirect(url_for('heroes'))

    return render_template('addhero.html', form=form)


@app.route('/talents')
def talents():
    talents = None
    talents = models.Talent.query.order_by(models.Talent.name)
    return render_template('talents.html', talents=talents)


@app.route('/addtalent', methods=['GET', 'POST'])
def add_talent():
    form = forms.AddTalentForm()

    if form.validate_on_submit():
        lvl         = form.lvl.data
        name        = form.name.data
        description = form.description.data

        talent      = models.Talent(lvl, name, description)

        db.session.add(talent)
        db.session.commit()
        flash(u'Talent: %s successfully added!' % talent.name)

        return redirect(url_for('add_talent'))

    return render_template('addtalent.html', form=form)


@app.route('/heroes/edit/<hid>', methods=['GET', 'POST'])
def edit_hero(hid):
    hero = None

    hero = models.Hero.query.filter_by(id=hid).first()
    

    if hero is None:
        return redirect(url_for('heroes'))

    form = forms.EditHeroForm(name=hero.name, role=hero.role, 
            description=hero.description, lore=hero.lore, hp=hero.hp, 
            hp_per_level=hero.hp_per_level, hp_regen=hero.hp_regen,
            hp_regen_per_level=hero.hp_regen_per_level, mana=hero.mana,
            mana_per_level=hero.mana_per_level, mana_regen=hero.mana_regen,
            mana_regen_per_level=hero.mana_regen_per_level, 
            attack_damage=hero.attack_damage, 
            attack_damage_per_level=hero.attack_damage_per_level, 
            attack_speed=hero.attack_speed)

    if form.validate_on_submit():
        hero.name                       = form.name.data
        hero.role                       = form.role.data
        hero.description                = form.description.data
        hero.lore                       = form.lore.data
        hero.hp                         = form.hp.data
        hero.hp_per_level               = form.hp_per_level.data
        hero.hp_regen                   = form.hp_regen.data
        hero.hp_regen_per_level         = form.hp_regen_per_level.data
        hero.mana                       = form.mana.data
        hero.mana_per_level             = form.mana_per_level.data
        hero.mana_regen                 = form.mana_regen.data
        hero.mana_regen_per_level       = form.mana_regen_per_level.data
        hero.attack_damage              = form.attack_damage.data
        hero.attack_damage_per_level    = form.attack_damage_per_level.data
        hero.attack_speed               = form.attack_speed.data
        db.session.commit()

        return redirect(url_for('heroes'))

    return render_template('edithero.html', hero=hero, form=form)


@app.route('/talents/edit/<tid>', methods=['GET', 'POST'])
def edit_talent(tid):
    talent = None

    talent = models.Talent.query.filter_by(id=tid).first()

    if talent is None:
        return redirect(url_for('talents'))

    form = forms.EditTalentForm(lvl=talent.lvl, name=talent.name, 
                                description=talent.description)

    if form.validate_on_submit():
        talent.lvl          = form.lvl.data
        talent.name         = form.name.data
        talent.description  = form.description.data
        db.session.commit()

        return redirect(url_for('talents'))

    return render_template('edittalent.html', talent=talent, form=form)


@app.route('/linktalent', methods=['GET', 'POST'])
def link_talent():
    heroes              = models.Hero.query.order_by(models.Hero.name)
    talents             = models.Talent.query.order_by(models.Talent.name)
    form                = forms.LinkTalentForm()
    form.talent.choices = [(g.id, g.name) for g in talents]
    form.hero.choices   = [(g.id, g.name) for g in heroes]

    if form.validate_on_submit():
        talent  = models.Talent.query.filter_by(id=form.talent.data).first()
        hero    = models.Hero.query.filter_by(id=form.hero.data).first()
        hero.talents.append(talent)
        db.session.commit()
        flash(u'%s linked succesfully linked to %s!' % (talent.name, hero.name))
        return redirect(url_for('link_talent'))
    """
    talents = hero.talents
    talent = models.Talent.query.filter_by(id=1).first()
    heroes = talent.heroes

    for i in heroes:
        print i
    for i in talents:
        print i
    """
    return render_template('linktalent.html', form=form)


@app.route('/abilities')
def abilities():
    abilities = None
    abilities = models.Ability.query.order_by(models.Ability.name)
    return render_template('abilities.html', abilities=abilities)


@app.route('/addability', methods=['GET', 'POST'])
def add_ability():
    heroes              = models.Hero.query.order_by(models.Hero.name)
    form                = forms.AddAbilityForm()
    form.heroes.choices = [(g.id, g.name) for g in heroes]

    if form.validate_on_submit():
        hero_id     = form.heroes.data
        name        = form.name.data
        description = form.description.data
        manacost    = form.manacost.data
        cooldown    = form.manacost.data
        type        = form.type.data
        key         = form.key.data

        ability     = models.Ability(hero_id, name, description, manacost, 
                                     cooldown, type, key)
        db.session.add(ability)
        db.session.commit()
        flash(u'Ability: %s successfully added.' % ability.name)

        return redirect(url_for('add_ability'))

    return render_template('addability.html', form=form)


@app.route('/abilities/edit/<aid>', methods=['GET', 'POST'])
def edit_ability(aid):
    ability = None
    ability = models.Ability.query.filter_by(id=aid).first()

    if ability is None:
        return redirect(url_for('abilities'))

    form = forms.EditAbilityForm(name=ability.name, 
            description=ability.description, manacost=ability.manacost, 
            cooldown=ability.cooldown, type=ability.type, key=ability.key)

    if form.validate_on_submit():
        ability.name        = form.name.data
        ability.description = form.description.data
        ability.manacost    = form.manacost.data
        ability.cooldown    = form.cooldown.data
        ability.type        = form.type.data
        ability.key         = form.key.data
        db.session.commit()

        return redirect(url_for('abilities'))

    return render_template('editability.html', ability=ability, form=form)


@app.route('/traits')
def traits():
    traits = None
    traits = models.Trait.query.order_by(models.Trait.name)
    return render_template('traits.html', traits=traits)


@app.route('/addtrait', methods=['GET', 'POST'])
def add_trait():
    heroes              = models.Hero.query.order_by(models.Hero.name)
    form                = forms.AddTraitForm()
    form.heroes.choices = [(g.id, g.name) for g in heroes]

    if form.validate_on_submit():
        hero_id     = form.heroes.data
        name        = form.name.data
        description = form.description.data
        manacost    = form.manacost.data
        cooldown    = form.manacost.data
        type        = form.type.data
        key         = form.key.data

        ability     = models.Trait(hero_id, name, description, manacost, 
                                   cooldown, type, key)
        db.session.add(ability)
        db.session.commit()

        return redirect(url_for('traits'))

    return render_template('addtrait.html', form=form)


@app.route('/traits/edit/<tid>', methods=['GET', 'POST'])
def edit_trait(tid):
    ability = None
    ability = models.Trait.query.filter_by(id=tid).first()

    if ability is None:
        return redirect(url_for('abilities'))

    form = forms.EditTraitForm(name=ability.name, 
            description=ability.description, manacost=ability.manacost, 
            cooldown=ability.cooldown, type=ability.type, key=ability.key)

    if form.validate_on_submit():
        ability.name        = form.name.data
        ability.description = form.description.data
        ability.manacost    = form.manacost.data
        ability.cooldown    = form.cooldown.data
        ability.type        = form.type.data
        ability.key         = form.key.data
        db.session.commit()

        return redirect(url_for('traits'))

    return render_template('edittrait.html', ability=ability, form=form)


@app.route('/heroicabilities')
def heroics():
    heroics = None
    heroics = models.HeroicAbility.query.filter_by(type="Heroic Ability").order_by(models.HeroicAbility.name)
    return render_template('heroics.html', heroics=heroics)


@app.route('/addheroic', methods=['GET', 'POST'])
def add_heroic():
    heroes              = models.Hero.query.order_by(models.Hero.name)
    form                = forms.AddHeroicForm()
    form.heroes.choices = [(g.id, g.name) for g in heroes]

    if form.validate_on_submit():
        hero_id     = form.heroes.data
        name        = form.name.data
        description = form.description.data
        manacost    = form.manacost.data
        cooldown    = form.manacost.data
        type        = form.type.data
        key         = form.key.data

        ability     = models.HeroicAbility(hero_id, name, description, manacost, 
                                           cooldown, type, key)
        db.session.add(ability)
        db.session.commit()

        return redirect(url_for('heroics'))

    return render_template('addheroic.html', form=form)


@app.route('/heroics/edit/<hid>', methods=['GET', 'POST'])
def edit_heroic(hid):
    heroic = None
    heroic = models.HeroicAbility.query.filter_by(id=hid).first()

    if heroic is None:
        return redirect(url_for('heroics'))

    form = forms.EditHeroicForm(name=heroic.name, 
            description=heroic.description, manacost=heroic.manacost, 
            cooldown=heroic.cooldown, type=heroic.type, key=heroic.key)

    if form.validate_on_submit():
        heroic.name = form.name.data
        heroic.description = form.description.data
        heroic.manacost = form.manacost.data
        heroic.cooldown = form.cooldown.data
        heroic.type = form.type.data
        heroic.key = form.key.data
        db.session.commit()

        return redirect(url_for('heroics'))

    return render_template('editheroic.html', heroic=heroic, form=form)


@app.route('/heroes/<hid>', methods=['GET', 'POST'])
def hero(hid):
    hero = None
    hero = models.Hero.query.filter_by(id=hid).first()

    if hero is None:
        return redirect(url_for('heroes'))

    sql = "select * from talents t where t.id in (select talent_id from hero_talents where hero_id=%s) order by lvl;" % hid
    talents = db.engine.execute(sql)
    abilities = models.Ability.query.filter_by(hero_id=hid).order_by(models.Ability.name)
    print abilities

    return render_template('hero.html', hero=hero, talents=talents, abilities=abilities)


@app.route('/unlink/<hid>/<tid>', methods=['GET', 'POST'])
def unlink(hid, tid):
    db.engine.execute("DELETE FROM hero_talents WHERE hero_id=%s AND talent_id=%s" % (hid, tid))
    db.session.commit()
    return redirect(url_for('heroes', hid=hid))
