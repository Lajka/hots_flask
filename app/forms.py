from flask import url_for, redirect
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, BooleanField, FloatField, TextAreaField, IntegerField, SelectField
from wtforms.validators import Required, Length, EqualTo, InputRequired
import models

class TestForm(Form):
    name = StringField(u'Name', validators=[Required()])
    role = StringField(u'Role', validators=[Required()])
    submit = SubmitField(u'Do it')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None


class AddHeroForm(Form):
    name = StringField(u'Name', validators=[Required()])
    role = StringField(u'Role', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    lore = TextAreaField(u'Lore', validators=[Required()])
    hp = IntegerField(u'Health', validators=[Required(message="Must be an integer.")])
    hp_per_level = IntegerField(u'Health per level', validators=[Required()])
    hp_regen = FloatField(u'Health regen', validators=[Required()])
    hp_regen_per_level = FloatField(u'Health regen per level', validators=[Required()])
    mana = IntegerField(u'Mana', validators=[InputRequired()])
    mana_per_level = IntegerField(u'Mana per level', validators=[InputRequired()])
    mana_regen = FloatField(u'Mana regen', validators=[InputRequired()])
    mana_regen_per_level = FloatField(u'Mana regen per level', 
                                      validators=[InputRequired()])
    attack_damage = IntegerField(u'Attack damage', validators=[Required()])
    attack_damage_per_level = IntegerField(u'Attack damage per level', 
                                           validators=[Required()])
    attack_speed = FloatField(u'Attack speed', validators=[Required()])
    submit = SubmitField(u'Add hero')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        hero = models.Hero.query.filter_by(name=self.name.data).first()

        if hero is None:
            return True
        else:
            self.name.errors.append(u'Hero with that name already exists.')
            return False


class EditHeroForm(Form):
    name = StringField(u'Name', validators=[Required()])
    role = StringField(u'Role', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    lore = TextAreaField(u'Lore', validators=[Required()])
    hp = IntegerField(u'Health', validators=[Required(message="Must be an integer.")])
    hp_per_level = IntegerField(u'Health per level', validators=[Required()])
    hp_regen = FloatField(u'Health regen', validators=[Required()])
    hp_regen_per_level = FloatField(u'Health regen per level', validators=[Required()])
    mana = IntegerField(u'Mana', validators=[InputRequired()])
    mana_per_level = IntegerField(u'Mana per level', validators=[InputRequired()])
    mana_regen = FloatField(u'Mana regen', validators=[InputRequired()])
    mana_regen_per_level = FloatField(u'Mana regen per level', 
                                      validators=[InputRequired()])
    attack_damage = IntegerField(u'Attack damage', validators=[Required()])
    attack_damage_per_level = IntegerField(u'Attack damage per level', 
                                           validators=[Required()])
    attack_speed = FloatField(u'Attack speed', validators=[Required()])

    submit = SubmitField(u'Save changes')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class AddTalentForm(Form):
    lvl = IntegerField(u'Level', validators=[InputRequired()])
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    submit = SubmitField(u'Add talent')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        talent = models.Talent.query.filter_by(name=self.name.data).first()

        if talent is None:
            return True
        else:
            self.name.errors.append(u'Talent with that name already exists.')
            return False


class EditTalentForm(Form):
    lvl = IntegerField(u'Level', validators=[InputRequired()])
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    submit = SubmitField(u'Save changes')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class LinkTalentForm(Form):
    talent = SelectField(u'Talent', coerce=int, validators=[Required()])
    hero = SelectField(u'Hero', coerce=int, validators=[Required()])
    submit = SubmitField(u'Link')
    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class AddAbilityForm(Form):
    heroes = SelectField(u'Hero', coerce=int, validators=[Required()])
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    manacost = IntegerField(u'Manacost')
    cooldown = IntegerField(u'Cooldown')
    type = StringField(u'Type')
    key = StringField(u'Key')
    submit = SubmitField(u'Add ability')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        ability = models.Ability.query.filter_by(name=self.name.data).first()

        if ability is None:
            return True
        else:
            self.name.errors.append(u'Ability with that name already exists.')
            return False


class EditAbilityForm(Form):
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    manacost = IntegerField(u'Manacost')
    cooldown = IntegerField(u'Cooldown')
    type = StringField(u'Type')
    key = StringField(u'Key')
    submit = SubmitField(u'Save changes')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class AddTraitForm(Form):
    heroes = SelectField(u'Hero', coerce=int, validators=[Required()])
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    manacost = IntegerField(u'Manacost')
    cooldown = IntegerField(u'Cooldown')
    type = StringField(u'Type', default="Trait")
    key = StringField(u'Key')
    submit = SubmitField(u'Add trait')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class EditTraitForm(Form):
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    manacost = IntegerField(u'Manacost')
    cooldown = IntegerField(u'Cooldown')
    type = StringField(u'Type')
    key = StringField(u'Key')
    submit = SubmitField(u'Save changes')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class AddHeroicForm(Form):
    heroes = SelectField(u'Hero', coerce=int, validators=[Required()])
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    manacost = IntegerField(u'Manacost')
    cooldown = IntegerField(u'Cooldown')
    type = StringField(u'Type', default="Heroic Ability")
    key = StringField(u'Key')
    submit = SubmitField(u'Add heroic ability')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)


class EditHeroicForm(Form):
    name = StringField(u'Name', validators=[Required()])
    description = TextAreaField(u'Description', validators=[Required()])
    manacost = IntegerField(u'Manacost')
    cooldown = IntegerField(u'Cooldown')
    type = StringField(u'Type')
    key = StringField(u'Key')
    submit = SubmitField(u'Save changes')

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
